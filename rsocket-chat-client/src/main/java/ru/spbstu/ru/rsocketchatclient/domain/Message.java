package ru.spbstu.ru.rsocketchatclient.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message  {
    private String id;
    private String chatId;
    private String message;
    private String senderId;
}
