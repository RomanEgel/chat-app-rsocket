package ru.spbstu.ru.rsocketchatclient.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Chat {
    private String id;
    private String senderId;
    private String receiverId;
}
