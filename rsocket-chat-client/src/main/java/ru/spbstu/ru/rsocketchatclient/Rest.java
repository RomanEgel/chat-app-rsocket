package ru.spbstu.ru.rsocketchatclient;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import ru.spbstu.ru.rsocketchatclient.domain.Chat;
import ru.spbstu.ru.rsocketchatclient.domain.Message;
import ru.spbstu.ru.rsocketchatclient.domain.User;

@RestController
@AllArgsConstructor
@Api
public class Rest {
    private final RSocketRequester rSocketRequester;


    @PostMapping("send-message")
    @ApiOperation("send message")
    void sendMessage(@RequestBody Message message) {
        rSocketRequester
                .route("send-message")
                .data(message)
                .send()
                .subscribe();
    }

    @GetMapping("open-chat/{chatId}")
    @ApiOperation("open chat")
    void openChat(@PathVariable String chatId) {
        rSocketRequester
                .route("open-chat")
                .data(chatId)
                .retrieveFlux(Message.class)
                .filter(m -> m.getSenderId() != null)
                .flatMap(message -> getUser(message.getSenderId())
                        .map(u -> {
                            String userName = u.getFirstName() + " " + u.getSecondName() + " : ";
                            message.setMessage(userName + message.getMessage());
                            return message;
                        }))
                .doFirst(() -> System.out.println("<-------CHAT STARTED------>"))
                .doOnNext(u -> System.out.println(u.getMessage()))
                .doOnComplete(() -> System.out.println("<-------CHAT ENDED------>"))
                .subscribe();
    }

    @PostMapping("create-user")
    @ApiOperation("create user")
    void createUser(@RequestBody User user) {
        rSocketRequester
                .route("create-user")
                .data(user)
                .retrieveMono(User.class)
                .doOnNext(u -> System.out.println(u.toString()))
                .subscribe();
    }


    @PostMapping("create-chat")
    @ApiOperation("create chat")
    void createChat(@RequestBody Chat chat) {
        rSocketRequester
                .route("create-chat")
                .data(chat)
                .retrieveMono(Chat.class)
                .doOnNext(u -> System.out.println(u.toString()))
                .subscribe();
    }

    @GetMapping("chat-list")
    @ApiOperation("chat list")
    void chatList() {
        rSocketRequester
                .route("chat-list")
                .retrieveFlux(Chat.class)
                .doOnNext(u -> System.out.println(u.toString()))
                .subscribe();
    }

    @GetMapping("user-list")
    @ApiOperation("user list")
    void userList() {
        rSocketRequester
                .route("user-list")
                .retrieveFlux(User.class)
                .doOnNext(u -> System.out.println(u.toString()))
                .subscribe();
    }

    private Mono<User> getUser(@PathVariable String userId) {
        return rSocketRequester
                .route("get-user")
                .data(userId)
                .retrieveMono(User.class);
    }
}
