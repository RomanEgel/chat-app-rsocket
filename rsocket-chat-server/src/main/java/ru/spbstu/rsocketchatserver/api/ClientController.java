package ru.spbstu.rsocketchatserver.api;

import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.spbstu.rsocketchatserver.domain.Chat;
import ru.spbstu.rsocketchatserver.domain.Message;
import ru.spbstu.rsocketchatserver.domain.User;
import ru.spbstu.rsocketchatserver.service.ClientService;

@Controller
@RequiredArgsConstructor
public class ClientController {
    private final ClientService clientService;

    @MessageMapping("send-message")
    public Mono<Void> sendMessage(Message message) {
        return clientService.sendMessage(message);
    }

    @MessageMapping("open-chat")
    public Flux<Message> openChat(String chatId) {
        return clientService.openChat(chatId);
    }

    @MessageMapping("create-user")
    public Mono<User> createUser(User user) {
        return clientService.createUser(user);
    }

    @MessageMapping("create-chat")
    public Mono<Chat> createUser(Chat chat) {
        return clientService.createChat(chat);
    }

    @MessageMapping("chat-list")
    public Flux<Chat> chatList() {
        return clientService.chatList();
    }

    @MessageMapping("user-list")
    public Flux<User> userList() {
        return clientService.userList();
    }

    @MessageMapping("get-user")
    public Mono<User> getUser(String userId) {
        return clientService.getUser(userId);
    }
}
