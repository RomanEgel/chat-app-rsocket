package ru.spbstu.rsocketchatserver.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import ru.spbstu.rsocketchatserver.domain.User;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User, String> {

}