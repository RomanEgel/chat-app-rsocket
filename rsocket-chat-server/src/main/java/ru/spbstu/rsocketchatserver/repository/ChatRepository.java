package ru.spbstu.rsocketchatserver.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import ru.spbstu.rsocketchatserver.domain.Chat;

@Repository
public interface ChatRepository extends ReactiveMongoRepository<Chat, String> {
}
