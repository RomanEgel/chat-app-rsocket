package ru.spbstu.rsocketchatserver.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.spbstu.rsocketchatserver.domain.Message;
import ru.spbstu.rsocketchatserver.domain.Chat;
import ru.spbstu.rsocketchatserver.domain.User;

public interface ClientService {
    Mono<Void> sendMessage(Message message);

    Flux<Message> openChat(String chatId);

    Mono<User> createUser(User user);

    Flux<Chat> chatList();

    Flux<User> userList();

    Mono<Chat> createChat(Chat chat);

    Mono<User> getUser(String userId);
}
