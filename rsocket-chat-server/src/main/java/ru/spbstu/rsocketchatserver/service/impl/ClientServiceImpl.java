package ru.spbstu.rsocketchatserver.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.spbstu.rsocketchatserver.domain.Message;
import ru.spbstu.rsocketchatserver.service.ClientService;
import ru.spbstu.rsocketchatserver.domain.Chat;
import ru.spbstu.rsocketchatserver.domain.User;
import ru.spbstu.rsocketchatserver.repository.ChatRepository;
import ru.spbstu.rsocketchatserver.repository.MessageRepository;
import ru.spbstu.rsocketchatserver.repository.UserRepository;

@Service
@AllArgsConstructor
public class ClientServiceImpl implements ClientService {
    private final MessageRepository clientRepo;
    private final UserRepository userRepo;
    private final ChatRepository chatRepo;

    @Override
    public Mono<Void> sendMessage(Message message) {
         return clientRepo.save(message)
                 .then();
    }

    @Override
    public Flux<Message> openChat(String chatId) {
        return clientRepo.findAllByChatId(chatId);
    }

    @Override
    public Mono<User> createUser(User user) {
        return userRepo.save(user);
    }

    @Override
    public Flux<Chat> chatList() {
        return chatRepo.findAll();
    }

    @Override
    public Flux<User> userList() {
        return userRepo.findAll();
    }

    @Override
    public Mono<Chat> createChat(Chat chat) {
        return chatRepo.save(chat);
    }

    @Override
    public Mono<User> getUser(String userId) {
        return userRepo.findById(userId);
    }
}
